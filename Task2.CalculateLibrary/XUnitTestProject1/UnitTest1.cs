using CalculateLibrary;
using System;
using Xunit;
using Xunit.Sdk;

namespace XUnitTestProject1
{
    public class UnitTest1:BeforeAfterTestAttribute
    {
        private static string strok5;
        private static string strok1;
        private static string strok2;
        private static string strok6;
        static  UnitTest1()
        {

            int i;
            string zero = "(((375293559706-375259523456+5+1)/32)*3561-2546+352684*255511+9)/100-333598/2+99-10000*78954-9999*(4664*3)-5456*56687/16+78*38*78*18+5756944+2214";
            string zero2 = "(1+1)/2+(0-1)*(34-330/10)";
            strok5 = "(1+1)";
            strok6 = "0";// "(1+1-22)/10*5";
            strok1 = zero;

            for (i = 0; i < 1000000 / zero.Length; i++)
                strok5 = strok5.Insert(strok5.Length, "+" + zero);
            strok5 = strok5.Insert(strok5.Length, "-1");

            for (i = 0; i < (1000000 - 10) / zero2.Length; i++)
                strok6 = strok6.Insert(strok6.Length, "+" + zero2);
            strok6 = strok6.Insert(strok6.Length, "+(1 + 1 - 22) / 10 * 5");
        }
    
     
    
        [Fact]
        public void Test1()
        {
            Assert.True(0 == Answer("(((375293559706-375259523456+5+1)/32)*3561-2546+352684*255511+9)/100-333598/2+99-10000*78954-9999*(4664*3)-5456*56687/16+78*38*78*18+5756944+2214"));
        }

        [Fact]
        public void TestSum()
        {
            Assert.True(7217 == Answer("124+2635+124+568+(234+3532)"));
        }

        [Fact]
        public void TestDifference()
        {
            Assert.True(339630623 == Answer("341151431-13461-1462457-83562-(34575-73247)"));
        }

        [Fact]
        public void TestMultiplication()
        {
            Assert.True(743758080 ==Answer("12*634*235*(52*8)"));
        }

        [Fact]
        public void TestDivision()
        {
            Assert.True(5 == Answer("12320*7875/4000/4851"));
        }

        [Fact]
        public void TestCalculate()
        {
            Assert.True(30 == Answer("(2+3)*6"));
            Assert.True(20 == Answer("2+3*6"));
        }

        [Fact]
        public void TestDivideByZeroException()
        {
            Assert.Throws< DivideByZeroException>(()=>Answer("23412/0"));
            Assert.Throws<DivideByZeroException>(() => Answer("63452/(12-(1376*0+14-2))"));
            Assert.Throws<DivideByZeroException>(() => Answer("2+4+235-14/(30-17-13)"));
        }

        [Fact]
        public void WrongInput()
        {
            Assert.Throws<FormatException>(() => Answer("��"));
        }

        [Fact(Timeout =60000)]
        public void TestLongTime()
        {

            Assert.True(0 == Answer(strok1));
        }

        [Fact(Timeout = 60000)]
        public void TestLongTimeSimpleString()
        {
            Assert.True(-10 == Answer(strok6));
        }




        [Fact(Timeout = 60000)]
        public void TestLongTimeComplexString()
        {
            // var t =Answer(strok5);
            Assert.True(1 == Answer(strok5));
        }

        public double Answer(string s)
        {
            return Calculator.Calculate(s);
        }
    }
}
