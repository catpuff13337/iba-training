﻿using System;
using System.Collections.Generic;

namespace CalculateLibrary
{
    public static class Calculator
    {
        /// <summary>
        /// Вычисление арифметического выражения
        /// </summary>
        /// <param name="example">Строка с примером</param>
        /// <returns>Возвращается число, которое является результатом вычисления</returns>
        public static Int64 Calculate(string example)
        {
            if (!IsCorrect(example))
            {
                throw new FormatException("The example contains extraneous characters.");
            }

            var separatingСhar = ',';
            var polishNotation = ReverseToPolishNotation(example, separatingСhar);
            var stack = new Stack<Int64>();
            var exampleElementsQueue = new Queue<string>(polishNotation.Split(separatingСhar));

            while (exampleElementsQueue.Count > 0)
            {
                var element = exampleElementsQueue.Dequeue();

                if (!char.IsDigit(element[0]) && char.TryParse(element, out char operatr))
                {
                    if (stack.Count > 1)
                    {
                        var b = stack.Pop();
                        var a = stack.Pop();
                        var c = Calculate(a, operatr, b);

                        stack.Push(c);
                    }
                    else if (stack.Count == 1)
                    {
                        if (operatr != '+')
                        {
                            var a = stack.Pop();

                            if (operatr == '-' && int.TryParse($"{-a}", out int c))
                            {
                                stack.Push(c);
                            }
                            else
                            {
                                throw new FormatException("The characters in the example are placed incorrectly.");
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("The example is incorrect.");
                    }
                }
                else if (Int64.TryParse(element, out Int64 operand))
                {
                    stack.Push(operand);
                }
            }

            if (stack.Count > 1)
            {
                throw new Exception("There are not enough operators in the example.");
            }
            else if (stack.Count < 1)
            {
                throw new Exception("There are not enough operands in the example.");
            }
            else
            {
                return stack.Pop();
            }
        }

        private static string ReverseToPolishNotation(string example, char separatingСhar)
        {
            var stack = new Stack<char>();
            var output = string.Empty;
            var number = string.Empty;
            var isUnaryOperator = true;
            var wasNumber = false;

            for (int i = 0; i < example.Length; i++)
            {
                var currentSymbol = example[i];

                // Если мы наткнулись на цифру
                if (char.IsDigit(currentSymbol))
                {
                    // Составляем число
                    number += currentSymbol;

                    // Если выбранный символ не последний
                    if (i + 1 < example.Length)
                    {
                        // Получаем следующий символ
                        var nextSymbol = example[i + 1];

                        // Если следующий символ - не цифра
                        if (!char.IsDigit(nextSymbol))
                        {
                            // Дописываем составленное число в выходную строку
                            output += $"{number}{separatingСhar}";
                            wasNumber = true;

                            // Очищаем строку для составления чисел
                            number = string.Empty;
                        }
                    }
                    // Если выбранный символ - последний
                    else if (i + 1 == example.Length)
                    {
                        // Дописываем составленное число в выходную строку
                        output += $"{number}{separatingСhar}";
                        wasNumber = true;

                        // Очищаем строку для составления чисел
                        number = string.Empty;
                    }
                }
                // Если мы наткнулись на операторы с приоритетом 1
                else if (currentSymbol == '+' || currentSymbol == '-')
                {
                    // Проверяем является ли символ унарным
                    if (isUnaryOperator && !wasNumber)
                    {
                        // Если символ оказывается унарным, то
                        // Добавляем ноль в выходную строку
                        output += $"{0}{separatingСhar}";
                        isUnaryOperator = false;
                    }

                    bool wasRemovedFromStackHighPriorityOperator;

                    // Повторяем цикл до тех пор, пока все высокоприоритетные операторы 
                    // не будут выброшены из стэка раньше низкоприоритетных
                    do
                    {
                        wasRemovedFromStackHighPriorityOperator = false;

                        // Проверяем все операторы в стеке до первой открывающейся скобки
                        foreach (var s in stack)
                        {
                            // Если наткнулись на открывающуюся скобку, 
                            // то прекращаем поиск высокоприоритетных операторов
                            if (s == '(')
                            {
                                break;
                            }
                            // Если наткнулись на высокроприоритетный оператор
                            else if (s == '+' || s == '-' || s == '*' || s == '/')
                            {
                                // Вытаскиваем высокоприоритетный символ из стэка
                                var higherPriorityOperator = stack.Pop();
                                wasRemovedFromStackHighPriorityOperator = true;

                                // И дописываем его в результирующую строку
                                output += $"{higherPriorityOperator}{separatingСhar}";
                                break;
                            }
                            // Если вдруг наткнулись на закрывающуюся скобку,
                            // значит пример составлен неверно
                            else if (s == ')')
                            {
                                throw new Exception("The example is incorrect.");
                            }
                        }
                    }
                    while (wasRemovedFromStackHighPriorityOperator);

                    stack.Push(currentSymbol);
                }
                // Если мы наткнулись на операторы с приоритетом 2
                else if (currentSymbol == '*' || currentSymbol == '/')
                {
                    // Проверяем все операторы в стеке до первой открывающейся скобки
                    foreach (var s in stack)
                    {
                        // Если наткнулись на открывающуюся скобку, 
                        // то прекращаем поиск высокоприоритетных операторов
                        if (s == '(')
                        {
                            break;
                        }
                        // Если наткнулись на оператор равного приоритета
                        else if (s == '*' || s == '/')
                        {
                            // Вытаскиваем оператор равного приоритета из стэка
                            var higherPriorityOperator = stack.Pop();

                            // И дописываем его в результирующую строку
                            output += $"{higherPriorityOperator}{separatingСhar}";
                            break;
                        }
                        // Если вдруг наткнулись на закрывающуюся скобку,
                        // значит пример составлен неверно
                        else if (s == ')')
                        {
                            throw new Exception("The example is incorrect.");
                        }
                    }

                    stack.Push(currentSymbol);

                    // Если наткнулись на высокоприоритетный оператор, значит следующий оператор может быть унарным
                    isUnaryOperator = true;
                    wasNumber = false;
                }
                // Если мы наткнулись на открывающуюся скобку
                else if (currentSymbol == '(')
                {
                    // Если открылась скобка, значит следующий оператор может быть унарным
                    isUnaryOperator = true;
                    wasNumber = false;

                    stack.Push(currentSymbol);
                }
                // Если наткнулись на закрывающуюся скобку
                else if (currentSymbol == ')')
                {
                    var isFoundOpeningBracket = false;

                    // Вытаскиваем все операторы до первой открывающейся скобки
                    // Или пока стэк не станет пустым
                    while (stack.Count > 0 && !isFoundOpeningBracket)
                    {
                        if (stack.Peek() == '(')
                        {
                            stack.Pop();
                            isFoundOpeningBracket = !isFoundOpeningBracket;
                        }
                        else
                        {
                            var oper = stack.Pop();
                            output += $"{oper}{separatingСhar}";
                        }
                    }

                    // Если в стэке операций не встретилась открывающаяся скобка,
                    // значит пример составлен неправильно
                    if (!isFoundOpeningBracket)
                    {
                        throw new FormatException("Brackets are placed incorrectly.");
                    }
                }
            }

            // Если в стэке остались операторы, вытаскиваем их оттуда
            while (stack.Count > 0)
            {
                var oper = stack.Pop();

                if (stack.Count > 0)
                {
                    output += $"{oper}{separatingСhar}";
                }
                else
                {
                    output += $"{oper}";
                }
            }

            if (output[output.Length - 1] == separatingСhar)
            {
                output = output.Remove(output.Length - 1, 1);
            }

            return output;
        }

        private static bool IsCorrect(string exampleString)
        {
            foreach (var symbol in exampleString)
            {
                //if (!char.IsDigit(symbol) &&
                //    !symbol.Equals('+') && !symbol.Equals('-') &&
                //    !symbol.Equals('*') && !symbol.Equals('/') &&
                //    !symbol.Equals('(') && !symbol.Equals(')'))
                if (char.IsLetter(symbol))
                {
                    return false;
                }
            }

            return true;
        }

        private static Int64 Calculate(Int64 a, char oper, Int64 b)
        {
            if (oper.Equals('+'))
            {
                return Addition(a, b);
            }
            else if (oper.Equals('-'))
            {
                return Subtraction(a, b);
            }
            else if (oper.Equals('*'))
            {
                return Multiplication(a, b);
            }
            else if (oper.Equals('/'))
            {
                return Division(a, b);
            }
            else
            {
                throw new Exception("Unknown operator.");
            }
        }

        private static Int64 Addition(Int64 a, Int64 b)
        {
            return a + b;
        }

        private static Int64 Subtraction(Int64 a, Int64 b)
        {
            return a - b;
        }

        private static Int64 Multiplication(Int64 a, Int64 b)
        {
            return a * b;
        }

        private static Int64 Division(Int64 a, Int64 b)
        {
            if (b == 0)
            {
                throw new DivideByZeroException();
            }

            return a / b;
        }
    }
}
