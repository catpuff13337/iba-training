﻿using System;
using System.Collections.Generic;

namespace CalculateLibrary.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            var examples = new List<string>()
            {
                "(((375293559706-375259523456+5+1)/32)*3561-2546+352684*255511+9)/100-333598/2+99-10000*78954-9999*(4664*3)-5456*56687/16+78*38*78*18+5756944+2214",
                "-(100+5-(-20)-3*(-5))/(-10)",  // 0,100,5,+,0,20,-,-,3,0,5,-,*,-,0,10,-,/,- = 14
                "1+2-3+4-5",                    // 1,2,+,3,-,4,+,5,- = -1
                "5*2+10",                       // 5,2,*,10,+ = 20
                "(6+10-4)/(1+1*2)+1",           // 6,10,+,4,-,1,1,2,*,+,/,1,+ = 5
                "(8+2*5)/(1+3*2-4)",            // 8,2,5,*,+,1,3,2,*,+,4,-,/ = 6
                "-3-(-5+4)*(9/(6-3))-5",        // 3,-,5,-,4,+,9,6,3,-,/,*,-,5,- = -5
                "(5)/(10)",                     // 5,10,/ = 0
                "2+2*2-3*3+3",                  // 2,2,2,*,+,3,3,*,-,3,+ = 0
                "2+3",                          // 2,3,+ = 5
                "(2+3)*6",                      // 2,3,+,6,* = 30
                "()2+3)*6",                     // 2,3,+,6,* = Error
                "2+-6",                         // 2,+,6,- = -4
                "-(33*11)/-11",                 // 33,11,*,/,-,11,- = Error
                "-(33*11)/(-11)",               // 33,11,*,11,-,/,- = 33
                "-2",                           // 2,- = -2
                "2//2",                         // 2,/,2,/ = Error
                "2+3*5",                        // 2,3,5,*,+ = 17
                "(-(+2-1))"                     // 2,+,1,-,-, = -1
            };

            Console.WriteLine("Examples:");

            foreach (var e in examples)
            {
                try
                {
                    Console.WriteLine($"{e} = {Calculator.Calculate(e)}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{e} = Error! {ex.Message}");
                }
            }

            Console.ReadKey();
        }
    }
}
